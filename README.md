# PYNDownloadManager

[![CI Status](https://img.shields.io/travis/Yonas/PYNDownloadManager.svg?style=flat)](https://travis-ci.org/Yonas/PYNDownloadManager)
[![Version](https://img.shields.io/cocoapods/v/PYNDownloadManager.svg?style=flat)](https://cocoapods.org/pods/PYNDownloadManager)
[![License](https://img.shields.io/cocoapods/l/PYNDownloadManager.svg?style=flat)](https://cocoapods.org/pods/PYNDownloadManager)
[![Platform](https://img.shields.io/cocoapods/p/PYNDownloadManager.svg?style=flat)](https://cocoapods.org/pods/PYNDownloadManager)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PYNDownloadManager is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PYNDownloadManager'
```

## Author

Yonas, 464514962@qq.com

## License

PYNDownloadManager is available under the MIT license. See the LICENSE file for more info.
