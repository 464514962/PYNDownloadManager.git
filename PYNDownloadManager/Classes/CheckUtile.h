//
//  CheckUtile.h
//  IBCDownLoadManager
//
//  Created by yonas on 2018/5/23.
//  Copyright © 2018年 yonas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckUtile : NSObject

/**
 校验URL

 @param url url
 @return 是否正确
 */
+ (BOOL)checkUrl:(NSString *)url;

/**
 校验文件完整性

 @param fromPath 文件所在路径
 @param md5 服务器端的md5值
 @return 与服务器MD5是否一致
 */
+ (BOOL)checkMD5WithFileWithPath:(NSString *)fromPath originalMD5:(NSString *)md5;

/**
 校验文件路径

 @param fromPath 文件所在路径
 @return 文件是否存在
 */
+ (BOOL)checkFileExistsWithPath:(NSString *)fromPath;
@end
