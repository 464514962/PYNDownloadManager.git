//
//  IBCDownLoadManager.h
//  IBCDownLoadManager
//
//  Created by yonas on 2018/5/23.
//  Copyright © 2018年 yonas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PYNDownloadProtocol.h"

@interface PYNDownLoadManager : NSObject<NSCopying>

+ (instancetype)sharedManager;


/**
 根据URL下载文件(最简单的方法)

 @param url Url
 */
- (void)downLoadWithUrl:(NSURL *)url;

/**
 根据URL下载文件

 @param url 下载地址
 @param targetPath 下载到目标路径,默认路径下载到cache目录，名字为文件名
 @param delegate 下载过程的代理方法
 */
- (void)downLoadWithUrl:(NSURL *)url targetPath:(NSString *)targetPath delegate:(id <PYNDownloadProtocol>)delegate;


/**
  根据URL下载文件

 @param url 下载地址
 @param targetPath 下载到目标路径,默认路径下载到cache目录，名字为文件名
 @param completionHandler 下载结束时的处理，包含错误信息+目标路径
 */
- (void)downLoadWithUrl:(NSURL *)url targetPath:(NSString *)targetPath completionHandler:(void (^)(NSError *error,NSString *filePath)) completionHandler;

/**
   根据URL下载文件

 @param url 下载地址
 @param targetPath 下载到目标路径,默认路径下载到cache目录，名字为文件名
 @param progressHandler 下载进度
 @param completionHandler 下载结束时的处理，包含错误信息+目标路径
 */
- (void)downLoadWithUrl:(NSURL *)url targetPath:(NSString *)targetPath progressHandler:(void (^)(NSProgress *progress))progressHandler completionHandler:(void (^)(NSError *error,NSString *filePath)) completionHandler;

/**
 取消对应URL的任务。
 @param url 下载地址
 */
- (void)cancelTaskWithUrl:(NSURL *)url;

/**
 停止对应URL的任务。
 @param url 下载地址
 */
- (void)pasueTaskWithUrl:(NSURL *)url;

/**
 重新开始下载暂停的任务，如果没有该任务，就直接开始下载，默认cache目录。
 @param url 下载地址
 */
- (void)restartTaskWithUrl:(NSURL *)url;

/**
 下载器的delegate，可以随时更改监听者。
 */
@property(nonatomic,weak) id <PYNDownloadProtocol> delegate;
/**
 下载器进度
 */
@property(nonatomic,assign,readonly) NSProgress *downLoadProgress;
@end
