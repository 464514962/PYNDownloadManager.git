//
//  NSString+MD5.m
//  IBCDownLoadManager
//
//  Created by yonas on 2018/5/24.
//  Copyright © 2018年 yonas. All rights reserved.
//

#import "NSString+MD5.h"
#import <CommonCrypto/CommonDigest.h>
@implementation NSString (MD5)
+ (NSString *)md5WithFilePath:(NSString *)path{
    NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:path];
    if(handle == nil)
    {
        return nil;
    }
    CC_MD5_CTX md5;
    
    CC_MD5_Init(&md5);
    
    BOOL done =NO;
    while(!done)
    {
        NSData* fileData = [handle readDataOfLength:256];
        CC_MD5_Update(&md5, [fileData bytes],(CC_LONG)[fileData length]);
        if([fileData length] == 0)
            done = YES;
    }
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5_Final(digest, &md5);
    NSString *finalStr = [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                                              
                                              digest[0], digest[1], digest[2], digest[3], digest[4], digest[5],
                                              
                                              digest[6], digest[7], digest[8], digest[9], digest[10], digest[11],
                                              
                                              digest[12], digest[13], digest[14], digest[15]];
    
    return finalStr;
}
@end
