//
//  IBCDownLoadError.h
//  IBCDownLoadManager
//
//  Created by yonas on 2018/5/24.
//  Copyright © 2018年 yonas. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PYNDownLoadError : NSObject

/**
 文件完整性校验未通过的错误

 @return error
 */
+ (NSError *)errorByFileNotComplete;

/**
 文件路径未找到

 @return error
 */
+ (NSError *)errorByTargetPathNotFound;

/**
 URL格式不正确
 
 @return error
 */
+ (NSError *)errorByUrlNotCorrect;

/**
 没有根据url找到下载任务。
 
 @return error
 */
+ (NSError *)errorByNotFoundDownloadTaskWithUrl;
@end
