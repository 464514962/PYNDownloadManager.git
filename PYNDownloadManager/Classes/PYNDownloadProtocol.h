//
//  PYNDownloadProtocol.h
//  IBCDownLoadManager
//
//  Created by yonas on 2018/5/23.
//  Copyright © 2018年 yonas. All rights reserved.
//  DownLoadManager的回调。

#import <Foundation/Foundation.h>
@protocol PYNDownloadProtocol <NSObject>


/**
 下载器将要开始下载

 @param url 下载地址
 @param path 存储到本地的目标地址
 */
- (void)ibcDownloadWillStartWithUrl:(NSURL *)url destinatePath:(NSString *)path;

/**
 下载器开始下载

 @param url 下载地址
 @param path 存储到本地的目标地址
 @param progress 下载的进度
 */
- (void)ibcDownloadDidStartWithUrl:(NSURL *)url destinatePath:(NSString *)path progress:(NSProgress *)progress;

/**
 下载器结束下载

 @param url 下载地址
 @param path 存储到本地的目标地址
 @param error 错误信息
 */
- (void)ibcDownloadEndWithUrl:(NSURL *)url destinatePath:(NSString *)path error:(NSError *)error;

/**
 下载器下载失败

 @param url 文件路径
 @param path 文件目标地址
 @param error 错误信息
 */
- (void)ibcDownloadFailureWithUrl:(NSURL *)url destinatePath:(NSString *)path error:(NSError *)error;
@end
