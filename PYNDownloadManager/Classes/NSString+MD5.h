//
//  NSString+MD5.h
//  IBCDownLoadManager
//
//  Created by yonas on 2018/5/24.
//  Copyright © 2018年 yonas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

/**
 文件完整性校验

 @param path 文件路径
 @return 返回大写的md5
 */
+ (NSString *)md5WithFilePath:(NSString *)path;
@end
