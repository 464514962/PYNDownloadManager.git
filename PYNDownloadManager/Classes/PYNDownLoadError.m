//
//  IBCDownLoadError.m
//  IBCDownLoadManager
//
//  Created by yonas on 2018/5/24.
//  Copyright © 2018年 yonas. All rights reserved.
//

#import "PYNDownLoadError.h"
static NSString *const IBCDownloadErrorDomain = @"com.yonasPan.ibcdownload";

typedef enum : NSUInteger {
    DownLoadErrorFileNotComplete = -5000,
    DownLoadErrorTimeOut,
    DownLoadErrorFileNotFound,
    DownLoadErrorUrlNotCorrect,
    DownLoadErrorNotFindDownloadTaskByUrl
} DownLoadError;

@implementation PYNDownLoadError
+ (NSError *)errorByFileNotComplete{
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey:@"IBCDownLoadmanagerError:文件完整性校验失败"};
    
    NSError *md5Error = [[NSError alloc]initWithDomain:IBCDownloadErrorDomain code:DownLoadErrorFileNotComplete userInfo:userInfo];
    
    return md5Error;
}

+ (NSError *)errorByTargetPathNotFound{
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey:@"IBCDownLoadmanagerError:文件路径未找到/不正确",NSLocalizedRecoverySuggestionErrorKey:@"请查看目标路径和文件名、扩展名是否指定"};
    NSError *error = [[NSError alloc]initWithDomain:IBCDownloadErrorDomain code:DownLoadErrorFileNotFound userInfo:userInfo];
    
    return error;
}

+ (NSError *)errorByUrlNotCorrect{
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey:@"IBCDownLoadmanagerError:目标文件URL不正确",NSLocalizedRecoverySuggestionErrorKey:@"请检查URL地址是否正确"};
    NSError *error = [[NSError alloc]initWithDomain:IBCDownloadErrorDomain code:DownLoadErrorUrlNotCorrect userInfo:userInfo];
    
    return error;
}

+ (NSError *)errorByNotFoundDownloadTaskWithUrl{
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey:@"IBCDownLoadmanagerWarning:未根据url找到相对应的下载任务"};
    NSError *error = [[NSError alloc]initWithDomain:IBCDownloadErrorDomain code:DownLoadErrorNotFindDownloadTaskByUrl userInfo:userInfo];
    
    return error;
}
@end
