//
//  CheckUtile.m
//  IBCDownLoadManager
//
//  Created by yonas on 2018/5/23.
//  Copyright © 2018年 yonas. All rights reserved.
//

#import "CheckUtile.h"
#import "NSString+MD5.h"
@implementation CheckUtile
+ (BOOL)checkUrl:(NSString *)url{
    
    NSString *regex = @"((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    BOOL isValid = [predicate evaluateWithObject:url];
    
    return isValid;
}

+ (BOOL)checkMD5WithFileWithPath:(NSString *)fromPath originalMD5:(NSString *)md5{
    NSString *localMd5 = [NSString md5WithFilePath:fromPath];
    if ([localMd5 isEqualToString:md5]){
        return YES;
    }
    return NO;
}
+ (BOOL)checkFileExistsWithPath:(NSString *)fromPath{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *array = [fromPath componentsSeparatedByString:@"/"];
    NSString *fileName = array.lastObject;
    if (fileName.pathExtension.length==0){
        return NO;
    }
    BOOL isDir;
    
    [fileManager fileExistsAtPath:[[[fromPath copy] stringByDeletingLastPathComponent]stringByAppendingString:@"/"] isDirectory:&isDir];
    return isDir;
}
@end
