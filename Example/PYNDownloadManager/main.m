//
//  main.m
//  PYNDownloadManager
//
//  Created by Yonas on 12/13/2018.
//  Copyright (c) 2018 Yonas. All rights reserved.
//

@import UIKit;
#import "PYNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PYNAppDelegate class]));
    }
}
