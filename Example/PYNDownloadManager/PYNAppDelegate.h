//
//  PYNAppDelegate.h
//  PYNDownloadManager
//
//  Created by Yonas on 12/13/2018.
//  Copyright (c) 2018 Yonas. All rights reserved.
//

@import UIKit;

@interface PYNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
